apiVersion: v1
kind: Template
labels:
  app: codimd
  template: codimd-secret
metadata:
  annotations:
    description: CodiMD - Secrets
    iconClass: icon-nodejs
    openshift.io/display-name: CodiMD
    tags: codimd
  name: codimd-secret
objects:
- apiVersion: v1
  kind: Secret
  metadata:
    annotations:
      template.openshift.io/expose-bluemind-password: "{.data['bluemind-password']}"
      template.openshift.io/expose-codimd-password: "{.data['codimd-password']}"
      template.openshift.io/expose-demo-password: "{.data['demo-password']}"
      template.openshift.io/expose-fusion-password: "{.data['fusion-password']}"
      template.openshift.io/expose-lemonldap-password: "{.data['lemonldap-password']}"
      template.openshift.io/expose-lemonldap-sessions-password: "{.data['lemonldap-sessions-password']}"
      template.openshift.io/expose-mediawiki-password: "{.data['mediawiki-password']}"
      template.openshift.io/expose-monitor-password: "{.data['monitor-password']}"
      template.openshift.io/expose-nextcloud-password: "{.data['nextcloud-password']}"
      template.openshift.io/expose-rocket-password: "{.data['rocket-password']}"
      template.openshift.io/expose-root-password: "{.data['root-password']}"
      template.openshift.io/expose-ssoapp-password: "{.data['ssoapp-password']}"
      template.openshift.io/expose-ssp-password: "{.data['ssp-password']}"
      template.openshift.io/expose-syncrepl-password: "{.data['syncrepl-password']}"
      template.openshift.io/expose-whitepages-password: "{.data['whitepages-password']}"
    name: openldap-${FRONTNAME}
  stringData:
    bluemind-password: "${OPENLDAP_BLUEMIND_PASSWORD}"
    codimd-password: "${OPENLDAP_CODIMD_PASSWORD}"
    demo-password: "${OPENLDAP_DEMO_PASSWORD}"
    fusion-password: "${OPENLDAP_FUSION_PASSWORD}"
    lemonldap-password: "${OPENLDAP_LEMONLDAP_PASSWORD}"
    lemonldap-sessions-password: "${OPENLDAP_LEMONLDAP_SESSIONS_PASSWORD}"
    mediawiki-password: "${OPENLDAP_MEDIAWIKI_PASSWORD}"
    monitor-password: "${OPENLDAP_MONITOR_PASSWORD}"
    nextcloud-password: "${OPENLDAP_NEXTCLOUD_PASSWORD}"
    rocket-password: "${OPENLDAP_ROCKET_PASSWORD}"
    root-password: "${OPENLDAP_ROOT_PASSWORD}"
    ssoapp-password: "${OPENLDAP_SSOAPP_PASSWORD}"
    ssp-password: "${OPENLDAP_SSP_PASSWORD}"
    syncrepl-password: "${OPENLDAP_SYNCREPL_PASSWORD}"
    whitepages-password: "${OPENLDAP_WHITEPAGES_PASSWORD}"
- apiVersion: v1
  kind: Secret
  metadata:
    annotations:
      template.openshift.io/expose-database_name: "{.data['database-name']}"
      template.openshift.io/expose-password: "{.data['database-password']}"
      template.openshift.io/expose-username: "{.data['database-user']}"
    name: codimd-${FRONTNAME}
  stringData:
    database-name: "${CODIMD_POSTGRES_DATABASE}"
    database-password: "${CODIMD_POSTGRES_PASS}"
    database-user: "${CODIMD_POSTGRES_USER}"
parameters:
- name: FRONTNAME
  description: The name identifier assigned to objects defined in this template
  displayName: Name
  required: true
  value: demo
- name: CODIMD_POSTGRES_DATABASE
  description: CodiMD Postgres database name
  displayName: CodiMD Postgres Database
  required: true
  value: codimd
- name: CODIMD_POSTGRES_PASS
  description: CodiMD Postgres service account password
  displayName: CodiMD Postgres Password
  from: '[a-zA-Z0-9]{40}'
  generate: expression
  required: true
- name: CODIMD_POSTGRES_USER
  description: CodiMD Postgres service account username
  displayName: CodiMD Postgres Username
  required: true
  value: sw33t
- name: OPENLDAP_BLUEMIND_PASSWORD
  description: OpenLDAP BlueMind Service Account Password
  displayName: OpenLDAP BlueMind Service Account Password
  from: '[a-zA-Z0-9]{40}'
  generate: expression
- name: OPENLDAP_CODIMD_PASSWORD
  description: OpenLDAP CodiMD Service Account Password
  displayName: OpenLDAP CodiMD Service Account Password
  from: '[a-zA-Z0-9]{40}'
  generate: expression
- name: OPENLDAP_DEMO_PASSWORD
  description: Default Password to set any LDAP user
  displayName: LDAP Demo Password
  required: true
  value: secret
- name: OPENLDAP_FUSION_PASSWORD
  description: OpenLDAP FusionDirectory Service Account Password
  displayName: OpenLDAP FusionDirectory Service Account Password
  from: '[a-zA-Z0-9]{40}'
  generate: expression
- name: OPENLDAP_MEDIAWIKI_PASSWORD
  description: OpenLDAP MediaWiki Service Account Password
  displayName: OpenLDAP MediaWiki Service Account Password
  from: '[a-zA-Z0-9]{40}'
  generate: expression
- name: OPENLDAP_LEMONLDAP_PASSWORD
  description: OpenLDAP LemonLDAP Service Account Password
  displayName: OpenLDAP LemonLDAP Service Account Password
  from: '[a-zA-Z0-9]{40}'
  generate: expression
- name: OPENLDAP_LEMONLDAP_SESSIONS_PASSWORD
  description: OpenLDAP LemonLDAP Sessions Service Account Password
  displayName: OpenLDAP LemonLDAP Sessions Service Account Password
  from: '[a-zA-Z0-9]{40}'
  generate: expression
- name: OPENLDAP_MONITOR_PASSWORD
  description: OpenLDAP Monitor Service Account Password
  displayName: OpenLDAP Monitor Service Account Password
  from: '[a-zA-Z0-9]{40}'
  generate: expression
- name: OPENLDAP_NEXTCLOUD_PASSWORD
  description: OpenLDAP Nextcloud Service Account Password
  displayName: OpenLDAP Nextcloud Service Account Password
  from: '[a-zA-Z0-9]{40}'
  generate: expression
- name: OPENLDAP_ROCKET_PASSWORD
  description: OpenLDAP Rocket Service Account Password
  displayName: OpenLDAP Rocket Service Account Password
  from: '[a-zA-Z0-9]{40}'
  generate: expression
- name: OPENLDAP_ROOT_PASSWORD
  description: OpenLDAP Root Password
  displayName: OpenLDAP Root Password
  from: '[a-zA-Z0-9]{40}'
  generate: expression
- name: OPENLDAP_SSOAPP_PASSWORD
  description: OpenLDAP SSO Applications Service Account Password
  displayName: OpenLDAP SSO Applications Service Account Password
  from: '[a-zA-Z0-9]{40}'
  generate: expression
- name: OPENLDAP_SSP_PASSWORD
  description: OpenLDAP SelfServicePassword Service Account Password
  displayName: OpenLDAP SelfServicePassword Service Account Password
  from: '[a-zA-Z0-9]{40}'
  generate: expression
- name: OPENLDAP_SYNCREPL_PASSWORD
  description: OpenLDAP Syncrepl Service Account Password
  displayName: OpenLDAP Syncrepl Service Account Password
  from: '[a-zA-Z0-9]{40}'
  generate: expression
- name: OPENLDAP_WHITEPAGES_PASSWORD
  description: OpenLDAP WhitePages Service Account Password
  displayName: OpenLDAP WhitePages Service Account Password
  from: '[a-zA-Z0-9]{40}'
  generate: expression
