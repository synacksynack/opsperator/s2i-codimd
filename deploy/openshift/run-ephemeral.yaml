apiVersion: v1
kind: Template
labels:
  app: codimd
  template: codimd-ephemeral
message: |-
  The following service(s) have been created in your project:
      https://codimd.${ROOT_DOMAIN} -- CodiMD
metadata:
  annotations:
    description: CodiMD - ephemeral
    iconClass: icon-nodejs
    openshift.io/display-name: CodiMD
    tags: codimd
  name: codimd-ephemeral
objects:
- apiVersion: v1
  kind: DeploymentConfig
  metadata:
    labels:
      name: openldap-${FRONTNAME}
    name: openldap-${FRONTNAME}
  spec:
    replicas: 1
    selector:
      name: openldap-${FRONTNAME}
    strategy:
      type: Recreate
    template:
      metadata:
        labels:
          name: openldap-${FRONTNAME}
      spec:
        dnsPolicy: ClusterFirst
        containers:
        - env:
          - name: OPENLDAP_BIND_LDAP_PORT
            value: "1389"
          - name: OPENLDAP_BIND_LDAPS_PORT
            value: "1636"
          - name: OPENLDAP_BLUEMIND_PASSWORD
            valueFrom:
              secretKeyRef:
                name: openldap-${FRONTNAME}
                key: bluemind-password
          - name: OPENLDAP_CODIMD_PASSWORD
            valueFrom:
              secretKeyRef:
                name: openldap-${FRONTNAME}
                key: codimd-password
          - name: OPENLDAP_DEBUG_LEVEL
            value: "${OPENLDAP_DEBUG_LEVEL}"
          - name: OPENLDAP_DEMO_PASSWORD
            valueFrom:
              secretKeyRef:
                name: openldap-${FRONTNAME}
                key: demo-password
          - name: OPENLDAP_FUSION_PASSWORD
            valueFrom:
              secretKeyRef:
                name: openldap-${FRONTNAME}
                key: fusion-password
          - name: OPENLDAP_HOST_ENDPOINT
            value: openldap-${FRONTNAME}
          - name: OPENLDAP_LEMONLDAP_PASSWORD
            valueFrom:
              secretKeyRef:
                name: openldap-${FRONTNAME}
                key: lemonldap-password
          - name: OPENLDAP_LEMONLDAP_HTTPS
            value: "yes"
          - name: OPENLDAP_LEMONLDAP_SESSIONS_PASSWORD
            valueFrom:
              secretKeyRef:
                name: openldap-${FRONTNAME}
                key: lemonldap-sessions-password
          - name: OPENLDAP_LEMON_HTTP_PORT
            value: "8080"
          - name: OPENLDAP_MEDIAWIKI_PASSWORD
            valueFrom:
              secretKeyRef:
                name: openldap-${FRONTNAME}
                key: mediawiki-password
          - name: OPENLDAP_MONITOR_PASSWORD
            valueFrom:
              secretKeyRef:
                name: openldap-${FRONTNAME}
                key: monitor-password
          - name: OPENLDAP_NEXTCLOUD_PASSWORD
            valueFrom:
              secretKeyRef:
                name: openldap-${FRONTNAME}
                key: nextcloud-password
          - name: OPENLDAP_ORG_SHORT
            value: "${ORG_NAME}"
          - name: OPENLDAP_ROCKET_PASSWORD
            valueFrom:
              secretKeyRef:
                name: openldap-${FRONTNAME}
                key: rocket-password
          - name: OPENLDAP_ROOT_DN_PREFIX
            value: cn=admin
          - name: OPENLDAP_ROOT_DN_SUFFIX
            value: "${BASE_SUFFIX}"
          - name: OPENLDAP_ROOT_DOMAIN
            value: "${ROOT_DOMAIN}"
          - name: OPENLDAP_ROOT_PASSWORD
            valueFrom:
              secretKeyRef:
                name: openldap-${FRONTNAME}
                key: root-password
          - name: OPENLDAP_SMTP_SERVER
            value: "${SMTP_RELAY}"
          - name: OPENLDAP_SSO_CLIENT_PASSWORD
            valueFrom:
              secretKeyRef:
                name: openldap-${FRONTNAME}
                key: ssoapp-password
          - name: OPENLDAP_SSP_PASSWORD
            valueFrom:
              secretKeyRef:
                name: openldap-${FRONTNAME}
                key: ssp-password
          - name: OPENLDAP_SYNCREPL_PASSWORD
            valueFrom:
              secretKeyRef:
                name: openldap-${FRONTNAME}
                key: syncrepl-password
          - name: OPENLDAP_WHITEPAGES_PASSWORD
            valueFrom:
              secretKeyRef:
                name: openldap-${FRONTNAME}
                key: whitepages-password
          imagePullPolicy: IfNotPresent
          livenessProbe:
            initialDelaySeconds: 30
            timeoutSeconds: 1
            tcpSocket:
              port: 1389
          name: openldap
          ports:
          - containerPort: 1389
            protocol: TCP
          - containerPort: 1636
            protocol: TCP
          readinessProbe:
            exec:
              command:
              - /bin/sh
              - "-i"
              - "-c"
              - /usr/local/bin/is-ready.sh
            initialDelaySeconds: 5
            timeoutSeconds: 1
          resources:
            limits:
              cpu: "${OPENLDAP_CPU_LIMIT}"
              memory: "${OPENLDAP_MEMORY_LIMIT}"
          volumeMounts:
          - name: conf
            mountPath: /etc/openldap
          - name: data
            mountPath: /var/lib/ldap
          - name: run
            mountPath: /run
        restartPolicy: Always
        volumes:
        - emptyDir: {}
          name: conf
        - emptyDir: {}
          name: data
        - emptyDir: {}
          name: run
    triggers:
    - type: ImageChange
      imageChangeParams:
        automatic: true
        containerNames:
        - openldap
        from:
          kind: ImageStreamTag
          name: ${OPENLDAP_IMAGESTREAM_TAG}
        lastTriggeredImage: ''
    - type: ConfigChange
- apiVersion: v1
  kind: Service
  metadata:
    name: openldap-${FRONTNAME}
  spec:
    ports:
    - name: ldap
      protocol: TCP
      port: 1389
      targetPort: 1389
      nodePort: 0
    - name: ldaps
      protocol: TCP
      port: 1636
      targetPort: 1636
      nodePort: 0
    selector:
      name: openldap-${FRONTNAME}
    type: ClusterIP
- apiVersion: v1
  kind: DeploymentConfig
  metadata:
    labels:
      name: lemon-${FRONTNAME}
    name: lemon-${FRONTNAME}
  spec:
    replicas: 1
    selector:
      name: lemon-${FRONTNAME}
    strategy:
      type: Rolling
    template:
      metadata:
        labels:
          name: lemon-${FRONTNAME}
      spec:
        dnsPolicy: ClusterFirst
        hostAliases:
        - ip: "127.0.0.1"
          hostnames:
          - "auth.${ROOT_DOMAIN}"
          - "manager.${ROOT_DOMAIN}"
          - "reload.${ROOT_DOMAIN}"
        containers:
        - env:
          - name: OPENLDAP_BASE
            value: "${BASE_SUFFIX}"
          - name: OPENLDAP_BIND_DN_PREFIX
            value: cn=lemonldap,ou=services
          - name: OPENLDAP_BIND_PW
            valueFrom:
              secretKeyRef:
                name: openldap-${FRONTNAME}
                key: lemonldap-password
          - name: OPENLDAP_CONF_DN_PREFIX
            value: ou=lemonldap,ou=config
          - name: OPENLDAP_DOMAIN
            value: "${ROOT_DOMAIN}"
          - name: OPENLDAP_HOST
            value: openldap-${FRONTNAME}
          - name: OPENLDAP_HTTP_PORT
            value: "8080"
          - name: OPENLDAP_PORT
            value: "1389"
          - name: OPENLDAP_PROTO
            value: ldap
          image: ' '
          imagePullPolicy: IfNotPresent
          livenessProbe:
            failureThreshold: 15
            httpGet:
              path: /
              port: 8080
            initialDelaySeconds: 30
            periodSeconds: 20
            timeoutSeconds: 8
          name: lemon
          ports:
          - containerPort: 8080
            protocol: TCP
          readinessProbe:
            httpGet:
              path: /
              port: 8080
            initialDelaySeconds: 5
            periodSeconds: 20
            timeoutSeconds: 5
          resources:
            limits:
              cpu: "${LEMON_CPU_LIMIT}"
              memory: "${LEMON_MEMORY_LIMIT}"
          volumeMounts:
          - name: apachesites
            mountPath: /etc/apache2/sites-enabled
          - name: etcconf
            mountPath: /etc/lemonldap-ng
        restartPolicy: Always
        volumes:
        - emptyDir: {}
          name: etcconf
        - emptyDir: {}
          name: apachesites
    triggers:
    - type: ImageChange
      imageChangeParams:
        automatic: true
        containerNames:
        - lemon
        from:
          kind: ImageStreamTag
          name: ${LEMON_IMAGESTREAM_TAG}
        lastTriggeredImage: ''
    - type: ConfigChange
- apiVersion: v1
  kind: Service
  metadata:
    name: lemon-${FRONTNAME}
  spec:
    ports:
    - name: sso
      port: 8080
      targetPort: 8080
    selector:
      name: lemon-${FRONTNAME}
- apiVersion: v1
  kind: Route
  metadata:
    name: lemon-${FRONTNAME}-reload
  spec:
    host: reload.${ROOT_DOMAIN}
    to:
      kind: Service
      name: lemon-${FRONTNAME}
    tls:
      insecureEdgeTerminationPolicy: Redirect
      termination: edge
- apiVersion: v1
  kind: Route
  metadata:
    name: lemon-${FRONTNAME}-manager
  spec:
    host: manager.${ROOT_DOMAIN}
    to:
      kind: Service
      name: lemon-${FRONTNAME}
    tls:
      insecureEdgeTerminationPolicy: Redirect
      termination: edge
- apiVersion: v1
  kind: Route
  metadata:
    name: lemon-${FRONTNAME}-auth
  spec:
    host: auth.${ROOT_DOMAIN}
    to:
      kind: Service
      name: lemon-${FRONTNAME}
    tls:
      insecureEdgeTerminationPolicy: Redirect
      termination: edge
- apiVersion: v1
  kind: DeploymentConfig
  metadata:
    labels:
      name: postgres-${FRONTNAME}
    name: postgres-${FRONTNAME}
  spec:
    replicas: 1
    selector:
      name: postgres-${FRONTNAME}
    strategy:
      type: Recreate
    template:
      metadata:
        labels:
          name: postgres-${FRONTNAME}
      spec:
        dnsPolicy: ClusterFirst
        containers:
        - env:
          - name: POSTGRESQL_USER
            valueFrom:
              secretKeyRef:
                name: codimd-${FRONTNAME}
                key: database-user
          - name: POSTGRESQL_PASSWORD
            valueFrom:
              secretKeyRef:
                name: codimd-${FRONTNAME}
                key: database-password
          - name: POSTGRESQL_DATABASE
            valueFrom:
              secretKeyRef:
                name: codimd-${FRONTNAME}
                key: database-name
          - name: POSTGRESQL_MAX_CONNECTIONS
            value: ${CODIMD_POSTGRES_MAX_CONNECTIONS}
          - name: POSTGRESQL_SHARED_BUFFERS
            value: ${CODIMD_POSTGRES_SHARED_BUFFERS}
          image: ' '
          imagePullPolicy: IfNotPresent
          livenessProbe:
            exec:
              command:
              - /bin/sh
              - '-i'
              - '-c'
              - pg_isready -h 127.0.0.1 -p 5432
            initialDelaySeconds: 30
            timeoutSeconds: 1
          name: postgres
          ports:
          - containerPort: 5432
            protocol: TCP
          readinessProbe:
            exec:
              command:
              - /bin/sh
              - '-i'
              - '-c'
              - psql -h 127.0.0.1 -U $POSTGRESQL_USER -q -d $POSTGRESQL_DATABASE -c 'SELECT 1'
            initialDelaySeconds: 5
            timeoutSeconds: 1
          resources:
            limits:
              cpu: "${CODIMD_POSTGRES_CPU_LIMIT}"
              memory: "${CODIMD_POSTGRES_MEMORY_LIMIT}"
          volumeMounts:
          - name: data
            mountPath: /var/lib/pgsql/data
        restartPolicy: Always
        volumes:
        - emptyDir: {}
          name: data
    triggers:
    - type: ImageChange
      imageChangeParams:
        automatic: true
        containerNames:
        - postgres
        from:
          kind: ImageStreamTag
          name: ${POSTGRES_IMAGESTREAM_TAG}
          namespace: ${POSTGRES_NAMESPACE}
        lastTriggeredImage: ''
    - type: ConfigChange
- apiVersion: v1
  kind: Service
  metadata:
    name: postgres-${FRONTNAME}
  spec:
    ports:
    - name: postgresql
      protocol: TCP
      port: 5432
      targetPort: 5432
      nodePort: 0
    selector:
      name: postgres-${FRONTNAME}
    type: ClusterIP
- apiVersion: v1
  kind: DeploymentConfig
  metadata:
    labels:
      name: codimd-${FRONTNAME}
    name: codimd-${FRONTNAME}
  spec:
    replicas: 1
    selector:
      name: codimd-${FRONTNAME}
    strategy:
      type: Recreate
    template:
      metadata:
        labels:
          name: codimd-${FRONTNAME}
      spec:
        dnsPolicy: ClusterFirst
        containers:
        - env:
          - name: CMD_IMAGE_UPLOAD_TYPE
            value: filesystem
          - name: CODIMD_DB_DRIVER
            value: postgres
          - name: CODIMD_DOMAIN
            value: codimd.${ROOT_DOMAIN}
          - name: LDAP_BASE
            value: "${BASE_SUFFIX}"
          - name: LDAP_BIND_PASS
            valueFrom:
              secretKeyRef:
                name: openldap-${FRONTNAME}
                key: codimd-password
          - name: LDAP_BIND_PREFIX
            value: "cn=codimd,ou=services"
          - name: LDAP_HOST
            value: openldap-${FRONTNAME}
          - name: LDAP_PROTO
            value: "ldap"
          - name: LDAP_PORT
            value: "1389"
          - name: LEMON_HOST
            value: auth.${ROOT_DOMAIN}
          - name: LEMON_PROTO
            value: https
          - name: POSTGRES_DB
            valueFrom:
              secretKeyRef:
                name: codimd-${FRONTNAME}
                key: database-name
          - name: POSTGRES_HOST
            value: postgres-${FRONTNAME}
          - name: POSTGRES_PASS
            valueFrom:
              secretKeyRef:
                name: codimd-${FRONTNAME}
                key: database-password
          - name: POSTGRES_USER
            valueFrom:
              secretKeyRef:
                name: codimd-${FRONTNAME}
                key: database-user
          image: ' '
          imagePullPolicy: IfNotPresent
          livenessProbe:
            failureThreshold: 5
            initialDelaySeconds: 60
            periodSeconds: 60
            successThreshold: 1
            tcpSocket:
              port: 3000
            timeoutSeconds: 3
          name: codimd
          ports:
          - containerPort: 3000
            protocol: TCP
          readinessProbe:
            failureThreshold: 5
            initialDelaySeconds: 60
            periodSeconds: 60
            successThreshold: 1
            tcpSocket:
              port: 3000
            timeoutSeconds: 3
          resources:
            limits:
              cpu: "${CODIMD_CPU_LIMIT}"
              memory: "${CODIMD_MEMORY_LIMIT}"
          volumeMounts:
          - name: saml
            mountPath: /opt/app-root/src/saml
          - name: uploads
            mountPath: /opt/app-root/src/public/uploads
        restartPolicy: Always
        volumes:
        - emptyDir: {}
          name: saml
        - emptyDir: {}
          name: uploads
    triggers:
    - type: ImageChange
      imageChangeParams:
        automatic: true
        containerNames:
        - codimd
        from:
          kind: ImageStreamTag
          name: codimd:${CODIMD_IMAGE_TAG}
        lastTriggeredImage: ''
    - type: ConfigChange
- apiVersion: v1
  kind: Service
  metadata:
    name: codimd-${FRONTNAME}
  spec:
    ports:
    - name: codimd
      port: 3000
      targetPort: 3000
    selector:
      name: codimd-${FRONTNAME}
- apiVersion: v1
  kind: Route
  metadata:
    annotations:
      haproxy.router.openshift.io/timeout: 3m
    name: codimd-${FRONTNAME}
  spec:
    host: codimd.${ROOT_DOMAIN}
    to:
      kind: Service
      name: codimd-${FRONTNAME}
    tls:
      insecureEdgeTerminationPolicy: Redirect
      termination: edge
parameters:
- name: FRONTNAME
  description: The name identifier assigned to objects defined in this template
  displayName: Name
  required: true
  value: demo
- name: CODIMD_CPU_LIMIT
  description: Maximum amount of CPU a CodiMD container can use
  displayName: CodiMD CPU Limit
  required: true
  value: 300m
- name: CODIMD_IMAGE_TAG
  description: The ImageStreamTag we should pull images from
  displayName: Tag
  required: true
  value: master
- name: CODIMD_MEMORY_LIMIT
  description: Maximum amount of memory a CodiMD container can use
  displayName: CodiMD Memory Limit
  required: true
  value: 512Mi
- name: CODIMD_POSTGRES_CPU_LIMIT
  description: Maximum amount of CPU a CodiMD database container can use
  displayName: CodiMD Postgres CPU Limit
  required: true
  value: 500m
- name: CODIMD_POSTGRES_MEMORY_LIMIT
  description: Maximum amount of memory a CodiMD database container can use
  displayName: CodiMD Postgres Memory Limit
  required: true
  value: 768Mi
- name: CODIMD_POSTGRES_MAX_CONNECTIONS
  description: Maximum amount of connections PostgreSQL should accept
  displayName: Maximum Postgres Connections
  required: true
  value: "100"
- name: CODIMD_POSTGRES_SHARED_BUFFERS
  displayName: Postgres Shared Buffer Amount
  description: Amount of Memory PostgreSQL should dedicate to Shared Buffers
  required: true
  value: 12MB
- name: LEMON_CPU_LIMIT
  description: Maximum amount of CPU a Lemon container can use
  displayName: Lemon CPU Limit
  required: true
  value: 300m
- name: LEMON_IMAGESTREAM_TAG
  description: The ImageStreamTag we should pull images from
  displayName: Tag
  required: true
  value: lemon:master
- name: LEMON_MEMORY_LIMIT
  description: Maximum amount of memory a Lemon container can use
  displayName: Lemon Memory Limit
  required: true
  value: 512Mi
- name: OPENLDAP_CONF_VOLUME_CAPACITY
  description: Volume space available for OpenLDAP configuration, e.g. 512Mi, 2Gi.
  displayName: OpenLDAP Config Volume Capacity
  required: true
  value: 512Mi
- name: OPENLDAP_CPU_LIMIT
  description: Maximum amount of CPU an OpenLDAP container can use
  displayName: OpenLDAP CPU Limit
  required: true
  value: 300m
- name: OPENLDAP_DATA_VOLUME_CAPACITY
  description: Volume space available for OpenLDAP database, e.g. 512Mi, 2Gi.
  displayName: OpenLDAP Data Volume Capacity
  required: true
  value: 8Gi
- name: OPENLDAP_DEBUG_LEVEL
  description: OpenLDAP log level
  displayName: LDAP Log Level
  required: true
  value: '256'
- name: OPENLDAP_IMAGESTREAM_TAG
  description: OpenLDAP Image Tag
  displayName: OpenLDAP ImageStream Tag
  required: true
  value: openldap:master
- name: OPENLDAP_MEMORY_LIMIT
  description: Maximum amount of memory an OpenLDAP container can use
  displayName: OpenLDAP Memory Limit
  required: true
  value: 512Mi
- name: POSTGRES_IMAGESTREAM_TAG
  description: PostgreSQL ImageStream Tag
  displayName: postgresql imagestream tag
  required: true
  value: postgresql:10
- name: POSTGRES_NAMESPACE
  description: The OpenShift Namespace where the Postgres ImageStream resides
  displayName: Postgres Namespace
  required: true
  value: openshift
- name: BASE_SUFFIX
  description: OpenLDAP base suffix
  displayName: LDAP Base Suffix
  required: true
  value: dc=demo,dc=local
- name: ORG_NAME
  description: Organization Display Name
  displayName: Organization Display Name
  required: true
  value: Demo
- name: ROOT_DOMAIN
  description: Root Domain
  displayName: Root Domain
  required: true
  value: demo.local
- name: SMTP_RELAY
  description: SMTP Relay
  displayName: SMTP Relay
  required: true
  value: smtp.demo.local
