FRONTNAME=opsperator
-include Makefile.cust

.PHONY: install
install:
	@@NODE_ENV=production npm install --unsafe-perm --ignore-scripts phantomjs-prebuilt@2.1.16
	@@NODE_ENV=build npm install --unsafe-perm webpack
	@@npm install --unsafe-perm
	@@npm run build
	@@sed -i '/"build":/d' package.json
	@@if ! chmod -R g=u . 2>/dev/null; then \
	    echo ignoring chmod return value; \
	fi

.PHONY: kubebuild
kubebuild: kubecheck
	@@for f in image git task-build task-scan pipeline pipelinerun; \
	    do \
		kubectl apply -f deploy/kubernetes/tekton-$$f.yaml; \
	    done

.PHONY: kubecheck
kubecheck:
	@@kubectl version >/dev/null 2>&1 || exit 42

.PHONY: kubedeploy
kubedeploy: kubecheck
	@@for f in postgres-secret postgres-deployment \
	    postgres-service deployment service; \
	    do \
		kubectl apply -f deploy/kubernetes/$$f.yaml; \
	    done

.PHONY: ocbuild
ocbuild: occheck
	@@oc process -f deploy/openshift/imagestream.yaml | oc apply -f-
	BRANCH=`git rev-parse --abbrev-ref HEAD`; \
	if test "$$GIT_DEPLOYMENT_TOKEN"; then \
	    oc process -f deploy/openshift/build-with-secret.yaml \
		-p "CODIMD_REPOSITORY_REF=$$BRANCH" \
		-p "GIT_DEPLOYMENT_TOKEN=$$GIT_DEPLOYMENT_TOKEN" \
		| oc apply -f-
	else \
	    oc process -f deploy/openshift/build.yaml \
		-p "CODIMD_REPOSITORY_REF=$$BRANCH" \
		| oc apply -f-
	fi

.PHONY: occheck
occheck:
	@@oc whoami >/dev/null 2>&1 || exit 42

.PHONY: occlean
occlean: occheck
	@@oc process -f deploy/openshift/run-persistent.yaml \
	    -p FRONTNAME=$(FRONTNAME) | oc delete -f- || true
	@@oc process -f deploy/openshift/secret.yaml \
	    -p FRONTNAME=$(FRONTNAME) | oc delete -f- || true

.PHONY: ocdemoephemeral
ocdemoephemeral: ocbuild
	@@if ! oc describe secret openldap-$(FRONTNAME) >/dev/null 2>&1; then \
	    oc process -f deploy/openshift/secret.yaml \
		-p FRONTNAME=$(FRONTNAME) | oc apply -f-; \
	fi
	@@oc process -f deploy/openshift/run-ephemeral.yaml \
	    -p FRONTNAME=$(FRONTNAME) | oc apply -f-

.PHONY: ocdemopersistent
ocdemopersistent: ocbuild
	@@if ! oc describe secret openldap-$(FRONTNAME) >/dev/null 2>&1; then \
	    oc process -f deploy/openshift/secret.yaml \
		-p FRONTNAME=$(FRONTNAME) | oc apply -f-; \
	fi
	@@oc process -f deploy/openshift/run-persistent.yaml \
	    -p FRONTNAME=$(FRONTNAME) | oc apply -f-

.PHONY: ocdemo
ocdemo: ocdemoephemeral

.PHONY: ocpurge
ocpurge: occlean
	@@oc process -f deploy/openshift/build.yaml | oc delete -f- || true
	@@oc process -f deploy/openshift/imagestream.yaml \
	    | oc delete -f- || true

.PHONY: jsonlint
jsonlint:
	@@find . -type f \
		-not -ipath "./node_modules/*" \
		-not -ipath "./.vscode/*" \
		\( -name "*.json" -o -name "*.json.*" \) \
	    | while read json; \
		do \
		    echo $$json ; jq . $$json; \
		done

.PHONY: postinstall
postinstall:
	@@if ! chmod -R g=u . 2>/dev/null; then \
	    echo ignoring chmod return value; \
	fi
	@@./bin/heroku

.PHONY: run
run:	start

.PHONY: start
start:
	@@./startup.sh

.PHONY: test
test:
	npm run-script lint \
	&& npm run-script jsonlint \
	&& ./node_modules/.bin/mocha

.PHONY: testdkr
testdkr:
	@@docker rm -f testpostgres || echo ok
	@@docker run --name testpostgres \
	   -e POSTGRESQL_USER=pguser \
	   -e POSTGRESQL_DATABASE=pgdb \
	   -e POSTGRESQL_PASSWORD=pgpassword \
	   -p 5432:5432 \
	   -d docker.io/centos/postgresql-12-centos7:latest
	@@docker rm -f testcodi || echo ok
	@@sleep 5; \
	pgip=`docker inspect testpostgres | awk '/"IPAddress": "/{print $$2}' | head -1 | cut -d'"' -f2`; \
	docker run --name testcodi \
	    -e POSTGRES_DB=pgdb \
	    -e POSTGRES_HOST=$$pgip \
	    -e POSTGRES_PASS=pgpassword \
	    -e POSTGRES_USER=pguser \
	    -e DEBUG=yay \
	    -d opsperator/codimd
