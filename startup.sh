#!/bin/sh

if test "$DEBUG"; then
    set -x
fi

CODIMD_ALLOW_GRAVATAR=${CODIMD_ALLOW_GRAVATAR:-true}
CODIMD_DB_DRIVER=${CODIMD_DB_DRIVER:-postgres}
CODIMD_DOMAIN=${CODIMD_DOMAIN:-codimd.demo.local}
CODIMD_ORG_NAME=${CODIMD_ORG_NAME:-KubeMD}
CODIMD_NODE_SSL=false
MYSQL_DB=${MYSQL_DB:-codimd}
MYSQL_HOST=${MYSQL_HOST:-localhost}
MYSQL_PASS=${MYSQL_PASS:-secret}
MYSQL_PORT=${MYSQL_PORT:-3306}
MYSQL_USER=${MYSQL_USER:-codimd}
OPENLDAP_BIND_DN_PREFIX="${OPENLDAP_BIND_DN_PREFIX:-cn=codimd,ou=services}"
OPENLDAP_BIND_PW="${OPENLDAP_BIND_PW:-secret}"
OPENLDAP_CONF_DN_PREFIX="${OPENLDAP_CONF_DN_PREFIX:-ou=lemonldap,ou=config}"
OPENLDAP_DOMAIN=${OPENLDAP_DOMAIN:-demo.local}
OPENLDAP_HOST=${OPENLDAP_HOST:-}
OPENLDAP_PROTO=${OPENLDAP_PROTO:-ldap}
OPENLDAP_USERS_OBJECTCLASS=${OPENLDAP_USERS_OBJECTCLASS:-inetOrgPerson}
if test -z "$OPENLDAP_BASE"; then
    OPENLDAP_BASE=`echo "dc=$OPENLDAP_DOMAIN" | sed 's|\.|,dc=|g'`
fi
if test -z "$OPENLDAP_PORT" -a "$OPENLDAP_PROTO" = ldaps; then
    OPENLDAP_PORT=636
elif test -z "$OPENLDAP_PORT"; then
    OPENLDAP_PORT=389
fi
if test -z "$LLNG_SSO_DOMAIN"; then
    LLNG_SSO_DOMAIN=auth.$OPENLDAP_DOMAIN
fi
POSTGRES_DB=${POSTGRES_DB:-codimd}
POSTGRES_HOST=${POSTGRES_HOST:-localhost}
POSTGRES_PASS=${POSTGRES_PASS:-secret}
POSTGRES_PORT=${POSTGRES_PORT:-5432}
POSTGRES_USER=${POSTGRES_USER:-codimd}
SESSION_LIFETIME=${SESSION_LIFETIME:-86400}
if test -z "$CODIMD_USE_SSL"; then
    if mount 2>/dev/null | grep kubernetes.io >/dev/null; then
	CODIMD_USE_SSL=true
    else
	CODIMD_USE_SSL=false
    fi
fi
if test -z "$CODIMD_DOMAIN"; then
    CODIMD_DOMAIN=codimd.$OPENLDAP_DOMAIN
fi
if test -s /ssl/server.crt -a -s /ssl/server.key -a -s /ssl/ca-chain.crt; then
    CODIMD_NODE_SSL=true
fi
if test "$S3_ACCESS_KEY" -a "$S3_SECRET_KEY" -a "$S3_HOST"; then
    S3_BUCKET=${S3_BUCKET:-codimd}
    S3_PORT=${S3_PORT:-8080}
    S3_PROTO=${S3_PROTO:-http}
    if test -z "$S3_IS_MINIO"; then
	S3_REGION=${S3_REGION:-us-east-1}
    elif test "$S3_PROTO" = https; then
	S3_SECURE=true
    else
	S3_SECURE=false
    fi
fi
if test -z "$SESSION_SECRET"; then
    SESSION_SECRET=`./bin/generate_session_secret 2>/dev/null`
fi
if test -z "$SESSION_SECRET"; then
    SESSION_SECRET=73200cd4360506898807dbb5c8cb6b7852ce7c27e6569d39b576e737d5824c0ba196f645c5e3c779cce99cbe052fd10e858ce94d1dc271e00d8e4375546e579c
fi

cat <<EOF >./config.json
{
    "production": {
	"allowGravatar": $CODIMD_ALLOW_GRAVATAR,
EOF
if test "$CSP_REPORT_URI"; then
    cat <<EOF >>./config.json
	"csp": {
	    "enable": true,
	    "reportURI": "$CSP_REPORT_URI"
	},
EOF
else
    export CMD_CSP_ENABLE=true
fi
if test "$CODIMD_DB_DRIVER" = mysql; then
    cpt=0
    if test "$MYSQL_USER" -a "$MYSQL_PASS"; then
	export CMD_DB_URL="mysql://$MYSQL_USER:$MYSQL_PASS@$MYSQL_HOST:$MYSQL_PORT/$MYSQL_DB"
    else
	export CMD_DB_URL="mysql://$MYSQL_HOST:$MYSQL_PORT/$MYSQL_DB"
    fi
    echo Waiting for MySQL backend ...
    while true
    do
	if echo SHOW TABLES | mysql -u "$MYSQL_USER" \
		-p "$MYSQL_PASS" -h "$MYSQL_HOST" \
		-p "$MYSQL_PORT" "$MYSQL_DB" >/dev/null 2>&1; then
	    echo " MySQL is alive!"
	    break
	elif test "$cpt" -gt 25; then
	    echo Could not reach MySQL >&2
	    exit 1
	fi
	sleep 5
	echo -n MySQL KO ...
	cpt=`expr $cpt + 1`
    done
    cat <<EOF >>./config.json
	"db": {
	    "database": "$MYSQL_DB",
	    "dialect": "$CODIMD_DB_DRIVER",
	    "host": "$MYSQL_HOST",
	    "password": "$MYSQL_PASS",
	    "port": $MYSQL_PORT,
	    "username": "$MYSQL_USER"
	},
EOF
elif echo "$CODIMD_DB_DRIVER" | grep -i postgre >/dev/null; then
    cpt=0
    CODIMD_DB_DRIVER=postgres
    export PGSSLMODE=${PGSSLMODE:-disable}
    if test "$POSTGRES_USER" -a "$POSTGRES_PASS"; then
	export CMD_DB_URL="postgres://$POSTGRES_USER:$POSTGRES_PASS@$POSTGRES_HOST:$POSTGRES_PORT/$POSTGRES_DB?sslmode=$PGSSLMODE"
    else
	export CMD_DB_URL="postgres://$POSTGRES_HOST:$POSTGRES_PORT/$POSTGRES_DB?sslmode=$PGSSLMODE"
    fi
    echo Waiting for Postgres backend ...
    while true
    do
	if test "$DEBUG"; then
	    if echo '\d' | PGPASSWORD="$POSTGRES_PASS" \
		    psql -U "$POSTGRES_USER" -h "$POSTGRES_HOST" \
		    -p "$POSTGRES_PORT" "$POSTGRES_DB"; then
		echo " Postgres is alive!"
		break
	    elif test "$cpt" -gt 25; then
		echo Could not reach Postgres >&2
		exit 1
	    fi
	elif echo '\d' | PGPASSWORD="$POSTGRES_PASS" \
		psql -U "$POSTGRES_USER" -h "$POSTGRES_HOST" \
		-p "$POSTGRES_PORT" "$POSTGRES_DB" >/dev/null 2>&1; then
	    echo " Postgres is alive!"
	    break
	elif test "$cpt" -gt 25; then
	    echo Could not reach Postgres >&2
	    exit 1
	fi
	sleep 5
	echo Postgres KO ...
	cpt=`expr $cpt + 1`
    done
    cat <<EOF >>./config.json
	"db": {
	    "database": "$POSTGRES_DB",
	    "dialect": "postgres",
	    "host": "$POSTGRES_HOST",
	    "password": "$POSTGRES_PASS",
	    "port": $POSTGRES_PORT,
	    "username": "$POSTGRES_USER"
	},
EOF
else
    cat <<EOF >>./config.json
	"db": {
	    "dialect": "sqlite",
	    "storage": "./db.codimd.sqlite"
	},
EOF
fi
cat <<EOF >>./config.json
	"domain": "$CODIMD_DOMAIN",
	"hsts": {
	    "enable": true,
	    "maxAgeSeconds": 31536000,
	    "includeSubdomains": false,
	    "preload": true
	},
EOF
if test "$OPENLDAP_HOST"; then
    echo Waiting for LDAP backend ...
    cpt=0
    while true
    do
	if ldapsearch -H $OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT \
		-D "$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE" \
		-b "ou=users,$OPENLDAP_BASE" \
		-w "$OPENLDAP_BIND_PW" \
		"(objectClass=$OPENLDAP_USERS_OBJECTCLASS)" >/dev/null 2>&1; then
	    echo " LDAP is alive!"
	    break
	elif test "$cpt" -gt 25; then
	    echo Could not reach OpenLDAP >&2
	    exit 1
	fi
	sleep 5
	echo LDAP KO ...
	cpt=`expr $cpt + 1`
    done
    cat <<EOF >>./config.json
	"ldap": {
	    "bindCredentials": "$OPENLDAP_BIND_PW",
	    "bindDn": "$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE",
	    "providerName": "$CODIMD_ORG_NAME",
	    "searchAttributes": ["uid","cn","mail","mailAlternateAddress","mailBackupAddress"],
	    "searchBase": "ou=users,$OPENLDAP_BASE",
	    "searchFilter": "(&(objectClass=$OPENLDAP_USERS_OBJECTCLASS)(uid={{username}})(!(pwdAccountLockedTime=*)))",
EOF
    if test -s /certs/ca.crt; then
	cat <<EOF >>./config.json
	    "tlsca": "/certs/ca.crt",
EOF
    fi
    cat <<EOF >>./config.json
	    "url": "$OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT",
	    "usernameField": "uid",
	    "useridField": "uid"
	},
EOF
    if test "$LLNG_PROTO"; then
	echo Waiting for LemonLDAP-NG backend ...
	cpt=0
	while true
	do
	    if curl "$LLNG_PROTO://$LLNG_SSO_DOMAIN/" -k -o- >/dev/null 2>&1; then
		echo " LemonLDAP-NG is alive!"
		break
	    elif test "$cpt" -gt 25; then
		curl -vv "$LLNG_PROTO://$LLNG_SSO_DOMAIN/" -k -o-
		echo "Could not reach LemonLDAP-NG" >&2
		exit 1
	    fi
	    sleep 5
	    echo LLNG KO ...
	    cpt=`expr $cpt + 1`
	done
	if ! test -s ./saml/saml-cert.pem; then
	    if ! myldapsearch -D "$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE" \
		-b "$OPENLDAP_CONF_DN_PREFIX,$OPENLDAP_BASE" \
		-w "$OPENLDAP_BIND_PW" \
		-H $OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT/ \
		    | grep -A32 '^description:: {samlServicePrivateKeySig}' \
		    | sed 's|description:: {samlServicePrivateKeySig}||' \
		    | awk '{if ($0 == "" || $0 ~ /description/) { exit 0; } print $0;}' \
		    >./saml/private.key 2>/dev/null; then
		cat <<EOF >&2
WARNING: Failed querying $OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT for SAML signing key
WARNING: would not be able to generate SAML signing certificate
EOF
	    elif ! openssl rsa -in ./saml/private.key \
		-modulus -noout >/dev/null 2>&1; then
		cat <<EOF >&2
WARNING: Invalid keys returned from LDAP
WARNING: would not be able to generate SAML signing certificate
EOF
	    else
		chmod 0600 ./saml/private.key
		openssl req -new -key ./saml/private.key \
		    -out ./saml/saml-cert.csr \
		    -subj "/CN=$CODIMD_DOMAIN"
		openssl x509 -req -days 3650 \
		    -in ./saml/saml-cert.csr \
		    -signkey ./saml/private.key \
		    -out ./saml/saml-cert.pem
	    fi
	    rm -f ./saml/private.key
	fi
	if test -s ./saml/saml-cert.pem; then
	    cat <<EOF
	"saml": {
	    "idpSsoUrl": "$LLNG_PROTO://$LLNG_SSO_DOMAIN/saml/singleSignOn",
	    "idpCert": "`pwd`/saml/saml-cert.pem",
	    "identifierFormat": "urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified",
	    "groupAttribute": "memberOf",
	    "attribute": {
	       "id": "uid",
	       "username": "uid",
	       "email": "mail"
	    }
	},
	"email": false,
EOF
	else
	    cat <<EOF
	"email": true,
EOF
	fi >>./config.json
    else
	cat <<EOF >>./config.json
	"email": true,
EOF
    fi
fi
if test "$S3_ACCESS_KEY" -a "$S3_SECRET_KEY" -a "$S3_HOST" -a \
	-x /usr/bin/s3cmd; then
    cpt=0
    echo -n "Waiting for s3 backend "
    sed -e "s|S3_ACCESS_KEY|$S3_ACCESS_KEY|g" \
	-e "s|S3_HOST|$S3_HOST|g" -e "s|S3_PORT|$S3_PORT|g" \
	-e "s|S3_SECRET_KEY|$S3_SECRET_KEY|g" \
	./s3cfg.sample >/tmp/.s3cfg
    while true
    do
	if s3cmd -c /tmp/.s3cfg ls; then
	    echo " s3 is alive!"
	    break
	elif test "$cpt" -gt 25; then
	    echo "Could not reach s3" >&2
	    exit 1
	fi
	sleep 5
	echo -n .
	cpt=`expr $cpt + 1`
    done
    if test "$S3_IS_MINIO"; then
	cat <<EOF >>./config.json
	"imageUploadType": "minio",
	"minio": {
	    "accessKeyId": "$S3_ACCESS_KEY",
	    "endPoint": "$S3_PROTO://$S3_HOST",
	    "port": $S3_PORT,
	    "secure": $S3_SECURE,
	    "secretKey": "$S3_SECRET_KEY"
	},
	"s3bucket": "$S3_BUCKET",
EOF
    else
	cat <<EOF >>./config.json
	"imageUploadType": "s3",
	"s3": {
	    "accessKeyId": "$S3_ACCESS_KEY",
	    "endpoint": "$S3_PROTO://$S3_HOST:$S3_PORT",
	    "region": "$S3_REGION",
	    "secretAccessKey": "$S3_SECRET_KEY"
	},
	"s3bucket": "$S3_BUCKET",
EOF
    fi
    rm -f /tmp/.s3cfg
    unset CMD_IMAGE_UPLOAD_TYPE
elif test "$CODIMD_IMGUR_CLIENT_ID"; then
    cat <<EOF >>./config.json
	"imageUploadType": "imgur",
	"imgur": {
	    "clientID": "$CODIMD_IMGUR_CLIENT_ID"
	},
EOF
    unset CMD_IMAGE_UPLOAD_TYPE
else
    export CMD_IMAGE_UPLOAD_TYPE=filesystem
fi
export CMD_AUTO_VERSION_CHECK=false
export CMD_HSTS_ENABLE=true
export CMD_HSTS_PRELOAD=true
if $CODIMD_NODE_SSL; then
    cat <<EOF >>./config.json
	"sslCertPath": "/ssl/server.crt",
	"sslKeyPath": "/ssl/server.key",
	"sslCAPath": "/ssl/ca-chain.crt",
EOF
fi
cat <<EOF >>./config.json
	"protocolUseSSL": $CODIMD_USE_SSL,
	"sessionLife": $SESSION_LIFETIME,
	"sessionSecret": "$SESSION_SECRET",
	"urlAddPort": false,
	"useCDN": false,
	"useSSL": $CODIMD_NODE_SSL
    }
}
EOF
cat <<EOF >./.sequelizerc
var path = require('path');

module.exports = {
    'config':          "`pwd`/config.json",
    'migrations-path': path.resolve('lib', 'migrations'),
    'models-path':     path.resolve('lib', 'models'),
    'url':             process.env.CMD_DB_URL
}
EOF

if ! ./node_modules/.bin/sequelize db:migrate; then
    echo WARNING: failed initializing database >&2
fi

exec pm2 start ecosystem.config.js --env k8s --no-daemon --no-vizion
