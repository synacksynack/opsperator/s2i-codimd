# k8s CodiMD

Forked from github.com/codimd/server, then moved to
https://github.com/hackmdio/codimd

See `minimal.patch` for an exhaustive overview of how we're patching upstream.

## Quick Start

To contribute, or just test this code on your local station, you may start
services with the following:

```
$ curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash
$ nvm install 12.13.0
$ nvm use 12.13.0
$ npm install -g pm2 node-gyp
$ npm install
$ export LDAP_HOST=1.2.3.4 LDAP_BASE=dc=demo,dc=local
$ pm2 start ecosystem.config.js
```

Environment variables and volumes
----------------------------------

The image recognizes the following environment variables that you can set during
initialization by passing `-e VAR=VALUE` to the Docker `run` command.

|    Variable name              |    Description             | Default                                                     |
| :---------------------------- | -------------------------- | ----------------------------------------------------------- |
|  `CODIMD_ALLOW_GRAVATAR`      | CodiMD Allow Gravatar      | `true`                                                      |
|  `CODIMD_DB_DRIVER`           | CodiMD Database Driver     | `postgres`                                                  |
|  `CODIMD_DOMAIN`              | CodiMD Site Domain         | `codimd.demo.local`                                         |
|  `CODIMD_ORG_NAME`            | CodiMD LDAP Provider Name  | `KubeMD`                                                    |
|  `CODIMD_IMGUR_CLIENT_ID`     | CodiMD imgur Client ID     | undef                                                       |
|  `OPENLDAP_BASE`              | OpenLDAP Base              | seds `OPENLDAP_DOMAIN`, default produces `dc=demo,dc=local` |
|  `OPENLDAP_BIND_DN_PREFIX`    | LDAP Bind Prefix           | `cn=codimd,ou=services`                                     |
|  `OPENLDAP_BIND_PW`           | LDAP Bind Password         | `secret`                                                    |
|  `OPENLDAP_DOMAIN`            | OpenLDAP Domain Name       | `demo.local`                                                |
|  `OPENLDAP_PORT`              | LDAP Directory Port        | `389`                                                       |
|  `OPENLDAP_PROTO`             | LDAP Directory Protocol    | `ldap`                                                      |
|  `OPENLDAP_USERS_OBJECTCLASS` | OpenLDAP Users ObjectClass | `inetOrgPerson`                                             |
|  `LLNG_PROTO`                 | SAML Backend Protocol      | unset, define to enable SSO configuration                   |
|  `LLNG_SSO_DOMAIN`            | SAML Backend Host          | `auth.$OPENLDAP_DOMAIN`                                     |
|  `MYSQL_DB`                   | CodiMD MySQL Database      | `codimd`                                                    |
|  `MYSQL_HOST`                 | CodiMD MySQL Host          | `localhost`                                                 |
|  `MYSQL_PASS`                 | CodiMD MySQL Password      | `secret`                                                    |
|  `MYSQL_PORT`                 | CodiMD MySQL Port          | `3306`                                                      |
|  `MYSQL_USER`                 | CodiMD MySQL User          | `codimd`                                                    |
|  `PGSSLMODE`                  | CodiMD Postgres SSL Mode   | `disable`                                                   |
|  `POSTGRES_DB`                | CodiMD Postgres Database   | `codimd`                                                    |
|  `POSTGRES_HOST`              | CodiMD Postgres Host       | `localhost`                                                 |
|  `POSTGRES_PASS`              | CodiMD Postgres Password   | `secret`                                                    |
|  `POSTGRES_PORT`              | CodiMD Postgres Port       | `5432`                                                      |
|  `POSTGRES_USER`              | CodiMD Postgres User       | `codimd`                                                    |
|  `S3_ACCESS_KEY`              | S3 ObjectStore Access Key  | unset                                                       |
|  `S3_BUCKET`                  | S3 ObjectStore Bucket      | `codimd`                                                    |
|  `S3_HOST`                    | S3 ObjectStore Proxy Host  | unset                                                       |
|  `S3_PORT`                    | S3 ObjectStore Proxy Port  | `8080`                                                      |
|  `S3_PROTO`                   | S3 ObjectStore Proxy Proto | `http`                                                      |
|  `S3_REGION`                  | S3 ObjectStore Region      | `us-east-1`                                                 |
|  `S3_SECRET_KEY`              | S3 ObjectStore Secret Key  | unset                                                       |
|  `SESSION_LIFETIME`           | CodiMD Sessions Lifetime   | `86400`                                                     |
|  `SESSION_SECRET`             | CodiMD Sessions Secret     | generated on boot                                           |


You can also set the following mount points by passing the `-v /host:/container`
flag to Docker.

|  Volume mount point                  | Description                              |
| :----------------------------------- | ---------------------------------------- |
|  `/certs`                            | CodiMD LDAP CA path (looks for `ca.crt`) |
|  `/opt/app-root/src/public/uploads`  | CodiMD Uploads Directory                 |
|  `/opt/app-root/src/saml`            | CodiMD SAML Certificates                 |
|  `/ssl`                              | CodiMD https Certificates                |


Ceph Integration/s3
--------------------

Switch from file (`/opt/app-root/src/public/uploads`) to s3 storage:

```
    -e S3_ACCESS_KEY=radosgw-access-key
    -e S3_BUCKET=my-s3-bucket
    -e S3_SECRET_KEY=radosgw-secret-key
    -e S3_HOST=radosgw.svc
    -e S3_PORT=8080
```

## FIXME

- test/validate s3 radosgw integration
- patch sessions management, allowing for scale outs
- EADDRINUSE forking to 2?
