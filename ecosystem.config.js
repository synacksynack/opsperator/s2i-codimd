let workersCount = Number(process.env.FORKS || 1);
module.exports = {
	apps : [
		{
		    name: 'front',
		    script: 'app.js',
		    instances: workersCount,
		    env_k8s: { 'NODE_ENV': 'production' },
		    env_production: { 'NODE_ENV': 'production' }
		}
	    ]
    };
